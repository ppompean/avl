#include<stdlib.h>

// TAB = array / stack
typedef struct _tab {
    int length, *tab;
} tab;

tab* init_tab();

void free_tab(tab *t);

void tab_push(tab *t, int val);

int tab_pop(tab *t);

int tab_peek(tab *t);

