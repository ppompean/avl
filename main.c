#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>

#include"tab.h"

typedef struct _tree {
    int val;
    struct _tree *l, *r, *father;
} tree;

static void balance(tree *root);

tree*
init_tree(tree *father, int val) {
    tree *t = malloc(sizeof(tree));
    t->val = val;
    t->father = father;
    t->l = t->r = NULL;
    return t;
}

void
tree_dispose(tree *root) {
    if(root) {
        tree_dispose(root->l);
        tree_dispose(root->r);
        free(root);
    }
}

// static functions can only be called in the file they are declared
// I prefix them by an underscore
void
tree_add(tree *root, int val) {
    if(val < root->val) {
        if(root->l) tree_add(root->l, val);
        else root->l = init_tree(root, val);
    }
    else if(val > root->val) {
        if(root->r) tree_add(root->r, val);
        else root->r = init_tree(root, val);
    }
    if(!root->father) balance(root);
}

static void
_tree_to_tab(tab *t, tree *root) {
    if(root->l) _tree_to_tab(t, root->l);
    tab_push(t, root->val);
    if(root->r) _tree_to_tab(t, root->r);
}

tab*
tree_to_tab(tree *root) {
    tab *t = init_tab();
    _tree_to_tab(t, root);
    return t;
}

tree*
tree_add_tab(tree *root, tab *t) {
    int i, len = t->length;
    for(i=0; i<len; i++) {
        tree_add(root, t->tab[i]);
    }
    return root;
}

// root2 can be null
void
tree_add_tree(tree *root, tree *root2) {
    if(root2) {
        tree_add(root, root2->val);
        tree_add_tree(root, root2->l);
        tree_add_tree(root, root2->r);
    }
}

tree*
tree_remove(tree *root, int val) {
    tree *keep = NULL;
    // If this node has to be removed
    if(val == root->val) {
        if(!root->l) {
            if(root->r) {
                root->r->father = root->father;
                keep = root->r;
            }
        } else {
            if(root->r) tree_add_tree(root->l, root->r);
            root->l->father = root->father;
            keep = root->l;
        }
        free(root);
        return keep;
    }
    // otherwise, proceed recursively
    if(val < root->val) {
        if(root->l) root->l = tree_remove(root->l, val);
    }
    else if(val > root->val) {
        if(root->r) root->r = tree_remove(root->r, val);
    }
    if(!root->father) balance(root);
    return root;
}

int
tree_contains(tree *root, int val) {
    return root->val == val
        || (root->l && root->val < val && tree_contains(root->l, val))
        || (root->r && root->val > val && tree_contains(root->r, val));
}

tree*
tree_get(tree *root, int val) {
    if(!root)
        return NULL;
    if(root->val < val)
        return tree_get(root->l, val);
    if(root->val > val)
        return tree_get(root->r, val);
    return root;
}

int
tree_length(tree *root) {
    if(!root) return 0;
    return 1 + tree_length(root->l) + tree_length(root->r);
}

static void
_tree_display(tree *root, tab *t) {
    int i;
    char *s;
    // left child
    tab_push(t, 1);
    if(root->l) _tree_display(root->l, t);
    tab_pop(t);
    // node itself
    for(i=0; i<t->length; i++) {
        // print '|' ssi tab[i] != tab[i-1]
        if(i > 0 && t->tab[i] != t->tab[i-1]) printf("│   ");
        else printf("    ");
    }
    // print '─' if root, '┌' if left, '└' if right
    s = t->length ? (tab_peek(t) ? "┌" : "└") : "─";
    printf("%s── %d\n", s, root->val);
    // right child
    tab_push(t, 0);
    if(root->r) _tree_display(root->r, t);
    tab_pop(t);
}

void
tree_display(tree *root) {
    tab *t = init_tab();
    _tree_display(root, t);
    printf("\n");
    free_tab(t);
}

static int
_max(int a, int b) {
    return a > b ? a : b;
}

int
tree_height(tree *root) {
    if(!root) return -1;
    return 1 + _max(tree_height(root->l), tree_height(root->r));
}

int
tree_max(tree *root) {
    if(!root->r) return root->val;
    return tree_max(root->r);
}

int
tree_min(tree *root) {
    if(!root->l) return root->val;
    return tree_min(root->l);
}

/*  - k
 *- a
 *  | - j
 *  - b
 *    - i
 *
 */
static void
right_rotation(tree* root) {
    int aval = aval = root->val;
    tree *b = root->l,
    *j = root->l->r;
    // swap root and left child vals without swapping structures
    root->val = b->val;
    b->val = aval;
    // swap childs
    b->r = root->r;
    root->r = b;
    root->l = root->l->l;
    b->l = j;
}

static void
left_rotation(tree* root) {
    int aval = aval = root->val;
    tree *b = root->r,
    *j = root->r->l;
    // swap root and right child vals without swapping structures
    root->val = b->val;
    b->val = aval;
    // swap childs
    b->l = root->l;
    root->l = b;
    root->r = root->r->r;
    b->r = j;
}

static void
balance(tree *root) {
    //balance childs
    if(root->l) balance(root->l);
    if(root->r) balance(root->r);
    // balance node
    // if difference between left element's tree_height and right element's tree_height > 2
    int hl = tree_height(root->l), hr = tree_height(root->r);
    int hcl, hcr;
    if(hl > hr+1) {
        hcl = tree_height(root->l->l);
        hcr = tree_height(root->l->r);
        if(hcl+1 < hcr) left_rotation(root->l);
        right_rotation(root);
    }
    else if(hl+1 < hr) {
        hcl = tree_height(root->r->l);
        hcr = tree_height(root->r->r);
        if(hcl > hcr+1) right_rotation(root->r);
        left_rotation(root);
    }
    //balance childs
    if(root->l) balance(root->l);
    if(root->r) balance(root->r);
}

void
test() {
    srand(time(NULL));
    int len = 20;
    tree *t = init_tree(NULL, rand()%100);
    /*
    tree_add(t, 10);
    tree_add(t, 30);
    tree_add(t, 5);
    tree_add(t, 15);
    tree_display(t);
    right_rotation(t);
    */
    tab *tab = init_tab();
    for(int i=0; i<len; i++) {
        tab_push(tab, rand()%1000);
        tree_add(t, tab_peek(tab));
        sleep(1);
        printf("\033[2J\033[1;1H");
        tree_display(t);
    }
    for(int i=0; i<len; i++) {
        t = tree_remove(t, tab_pop(tab));
        sleep(1);
        printf("\033[2J\033[1;1H");
        tree_display(t);
    }
}

int main() {
    test();
}
