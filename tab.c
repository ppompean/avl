#include"tab.h"

tab*
init_tab() {
    tab *t = malloc(sizeof(tab));
    t->tab = malloc(sizeof(int));
    t->length = 0;
    return t;
}

void
free_tab(tab *t) {
    free(t->tab);
    free(t);
}

void
tab_push(tab *t, int val) {
    t->length++;
    t->tab = realloc(t->tab, t->length*sizeof(int));
    t->tab[t->length-1] = val;
}

int
tab_pop(tab *t) {
    int remember = 0;
    if(t->length) {
        t->length--;
        remember = t->tab[t->length];
        t->tab = realloc(t->tab, t->length*sizeof(int));
    }
    return remember;
}

int
tab_peek(tab *t) {
    if(t->length) {
        return t->tab[t->length-1];
    }
    return 0;
}

